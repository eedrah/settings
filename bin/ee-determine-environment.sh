# Signal to show this script has run
export DETERMINED_ENVIRONMENT=""
DETERMINED_ENVIRONMENT+="Bash version $BASH_VERSION\n"

# Operating systems
[[ $OSTYPE == linux* ]] && export IS_LINUX=1 && DETERMINED_ENVIRONMENT+="Running Linux\n"
[[ $OSTYPE == darwin* ]] && export IS_MAC=1 && DETERMINED_ENVIRONMENT+="Running Mac\n"
[[ $OSTYPE == msys ]] && export IS_WINDOWS=1 && DETERMINED_ENVIRONMENT+="Running Windows\n"

[ -f /proc/1/cgroup  ] && awk -F/ '$2 == "docker"' /proc/1/cgroup | read && export IN_DOCKER_CONTAINER=1 && DETERMINED_ENVIRONMENT+="In Docker container\n"

# Setup up functions
if [[ $IS_LINUX ]]; then
  eegetDeviceSerialNumber () {
    [ $(command which udevadm) ] && info --query=all --name="$1" | sed -n -r 's/.*ID_SERIAL_SHORT=(.*)/\1/p'
  }
  eehash () {
    echo -n "$1" | md5sum | cut -f 1 -d ' '
  }
elif [[ $IS_MAC ]]; then
  eegetDeviceSerialNumber () {
    system_profiler -detailLevel full SPHardwareDataType | sed -n 's/.*Hardware UUID: \(.*\)/\1/p'
  }
  eehash () {
    md5 -qs "$1"
  }
elif [[ $IS_WINDOWS ]]; then
  eegetDeviceSerialNumber () {
    wmic bios get serialnumber
  }
  eehash () {
    echo -n "$1" | md5sum | cut -f 1 -d ' '
  }
fi

if [[ 098f6bcd4621d373cade4e832627b4f6 != $(eehash test) ]]; then
  echo Hashing test failed! Check the environment script.
fi

eehashDevice () {
  eehash $(eegetDeviceSerialNumber $1)
}

# Determine computers
[[ 513e6614c88d0d4c4af44d43b2d130ac == $(eehashDevice) ]] && export IS_COMPUTER_FEATHER=1 && DETERMINED_ENVIRONMENT+="On computer Feather\n"
[[ 559b0da031720b66d34e198d6ca4be95 == $(eehashDevice) ]] && export IS_COMPUTER_ANCHOR=1 && DETERMINED_ENVIRONMENT+="On computer Anchor\n"
[[ 531da3dbcc38a4541b8b744f0dfb6857 == $(eehashDevice /dev/sda) ]] && export IS_COMPUTER_LUZ=1 && DETERMINED_ENVIRONMENT+="On computer Luz\n"

[[ $IS_COMPUTER_FEATHER || $IS_COMPUTER_ANCHOR || $IS_COMPUTER_LUZ ]] && export IS_NOT_GUEST_COMPUTER=1

[[ ???? == $(eehashDevice /dev/sdb) ]] && export IS_HARDDRIVE_POTATO=sdb && DETERMINED_ENVIRONMENT+="With harddrive Potato\n"

# Git working directory
export GWD="/tmp/git"
[[ $IS_COMPUTER_FEATHER || $IS_COMPUTER_ANCHOR ]] && export GWD="$HOME/git"
[[ $IS_COMPUTER_LUZ ]] && export GWD="/mnt/sda3/git"

# Vim plugin directory
export VIM_PLUGINS_DIR='/tmp/.vim-plugins'
[[ $IS_COMPUTER_FEATHER || $IS_COMPUTER_ANCHOR ]] && export VIM_PLUGINS_DIR="$HOME/.vim-plugins"

