#! /bin/bash
set -eo pipefail

POSITIONAL=()
while
	key="$1"
	case $key in
		-p|--profile)
			PROFILE="$2"
			shift; shift ;;
		-P|--raw-profile)
			RAW_PROFILE="$2"
			shift; shift ;;
		-i|--incognito)
			INCOGNITO=1
			shift ;;
		-f|--file)
			FILE=1
			shift ;;
		-*)
			echo "Unknown option: $1"
			exit 1 ;;
		''|*)
			POSITIONAL+=("$1")
			shift ;;
	esac
	[[ $# -gt 0 ]]
do true; done
set -- "${POSITIONAL[@]}"

if [ -t 0 ]; then
	# Terminal input (keyboard) - interactive
	# Not pipe
	PAGE="$1"
else
	# File or pipe input - non-interactive
	PAGE="$(cat)"
fi

ARGS=()
if [ "$PROFILE" ]; then
	if [ "$IS_COMPUTER_FEATHER" ]; then
		case "$PROFILE" in
			"signed in"|"signed-in")
				RAW_PROFILE='Profile 1' ;;
			"browsing")
				RAW_PROFILE='Default' ;;
			"unseen engine"|"unseen-engine")
				RAW_PROFILE='Profile 2' ;;
			"nzp")
				RAW_PROFILE='Profile 4' ;;
		esac
	fi
fi
if [ "$PROFILE" ] && [ -z "$RAW_PROFILE" ]; then
    >&2 echo "Could not determine profile"
    exit 1
fi

[ "$RAW_PROFILE" ] && ARGS+=("--profile-directory=$RAW_PROFILE")
[ "$INCOGNITO" ] && ARGS+=("--incognito")
[ "$FILE" ] && PAGE="file://$PWD/$PAGE"

if [ "$IS_MAC" ]; then
	open -n -a "Google Chrome" --args "${ARGS[@]}" "$PAGE"
elif [ "$IS_WINDOWS" ]; then
	start chrome "${ARGS[@]}" "$PAGE"
else
    >&2 echo "Could not determine operating system"
    exit 1
fi