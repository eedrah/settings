if [[ -n $IS_WINDOWS ]]; then
    # To stop windows/msys being so slow, turn off some of the features

    #GIT_PS1_SHOWDIRTYSTATE=1
    #GIT_PS1_SHOWSTASHSTATE=1
    #GIT_PS1_SHOWUNTRACKEDFILES=1
    #GIT_PS1_SHOWFILECOUNT=1
    #GIT_PS1_SHOWUPSTREAM="auto"
    GIT_PS1_DESCRIBE_STYLE="branch"
    GIT_PS1_SHOWCOLORHINTS=1
else
    GIT_PS1_SHOWDIRTYSTATE=1
    GIT_PS1_SHOWSTASHSTATE=1
    GIT_PS1_SHOWUNTRACKEDFILES=1
    GIT_PS1_SHOWFILECOUNT=1
    GIT_PS1_SHOWUPSTREAM="auto verbose"
    GIT_PS1_DESCRIBE_STYLE="branch"
    GIT_PS1_SHOWCOLORHINTS=1
fi

source "$( dirname "${BASH_SOURCE[0]}" )/git-sh-prompt.sh"
#PROMPT_COMMAND='__git_ps1 "\u@\h:\w" "\\\$ "'
old_PS1="${PS1//\\$ /}"
PROMPT_COMMAND='__git_ps1 "$old_PS1" "\\n$(s=$? && [[ $s != 0 ]] && printf %s "\\[\\033[01;31m\\]$s ")$([ "$(jobs -p)" ] && printf %s "\\[\\033[01;35m\\]jobs ")\\[\\033[01;32m\\]\\!>\\[\\033[00m\\] "'
