#! /bin/bash
source "$( dirname "${BASH_SOURCE[0]}" )"/ssh-find-agent.sh

case "$1" in
    '') ;;
    --find) ;;
	--kill) ;;
    *)
        echo Not a valid parameter
        return 1
        ;;
esac

if [[ "$1" == "--find" ]]; then
  ssh-find-agent -a
  return 0
fi

if [[ "$1" == "--kill" ]]; then
	pkill ssh-agent
	return $?
fi

ssh-find-agent -a || eval $(ssh-agent -s) > /dev/null
for pubkey in $(/bin/ls -a $HOME/.ssh/*.pub); do
    privkey=${pubkey%.pub}
    if ! ssh-add -l | grep -q "$privkey"; then
      if [[ -n $1 ]]; then
          if grep ENCRYPTED "$privkey" > /dev/null; then
              echo Skipping "$privkey"
              continue
          fi
      fi
      ssh-add "$privkey"
    fi
done

