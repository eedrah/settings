"Because the plugins are loaded after .vimrc, these need to go here to
"override the mappings defined in the plugins

"Plug 'michaeljsmith/vim-indent-object'
onoremap ai :<C-U>call eval(printf('<SNR>%d_HandleTextObjectMapping(0, 1, 0, [line("."), line("."), col("."), col(".")])', GetScriptNumber("vim-indent-object")))<CR>
vnoremap ai :<C-U>call eval(printf('<SNR>%d_HandleTextObjectMapping(0, 1, 1, [line("''<"), line("''>"), col("''<"), col("''>")])', GetScriptNumber("vim-indent-object")))<CR><Esc>gv

"Plug 'jeetsukumaran/vim-indentwise'
onoremap <silent> <Plug>(IndentWiseNextEqualIndent)        V:<C-U>call eval(printf('<SNR>%d_move_to_indent_depth(1, "==", 0, "o")', GetScriptNumber("vim-indentwise")))<CR>

