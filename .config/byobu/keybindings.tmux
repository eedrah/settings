# To see the original:
#less $BYOBU_PREFIX/share/byobu/keybindings/f-keys.tmux

unbind-key -n C-a
set -g prefix ^A
set -g prefix2 ^A
bind a send-prefix

# Fixes for MacOS - python3 is in a different location, so all these kill the window if they have a -k
unbind-key -n F1
bind-key -n S-F1 new-window -n help "sh -c '$BYOBU_PAGER $BYOBU_PREFIX/share/doc/byobu/help.tmux.txt'"
bind-key -n S-F5 new-window "$BYOBU_PREFIX/lib/byobu/include/cycle-status" \; source $BYOBU_PREFIX/share/byobu/profiles/tmuxrc
unbind-key -n F9

unbind-key -n F5 # Get F5 working properly for terminal programs
unbind-key -n C-S-F5
bind-key -n C-S-F5 source $BYOBU_PREFIX/share/byobu/profiles/tmuxrc

# So that I can use navigation by word
unbind-key -n M-Up
unbind-key -n M-Down
unbind-key -n M-Left
unbind-key -n M-Right

# Swap F6 functions
bind-key -n F6 kill-pane
bind-key -n C-F6 detach

# Start with mouse on
# No idea why this is called 'disable' instead of 'enable'
source $BYOBU_PREFIX/share/byobu/keybindings/mouse.tmux.disable 

# Reset
unbind-key -n F1
unbind-key -n F2
unbind-key -n F3
unbind-key -n F4
unbind-key -n F5
unbind-key -n F6
unbind-key -n F7
unbind-key -n F8
unbind-key -n F9
unbind-key -n F10
unbind-key -n F11
unbind-key -n F12

bind-key -n F3 select-pane -t :.- \; display-panes
bind-key -n S-F3 split-window -v -c "#{pane_current_path}" \; display-panes

bind-key -n F4 select-pane -t :.+ \; display-panes
#bind-key -n S-F4 kill-pane \; display-panes
bind-key -n S-F4 swap-pane -s . -t :.+ \; display-panes

bind-key -n F5 previous-window \; display-panes
bind-key -n S-F5 new-window -c "#{pane_current_path}" \; command-prompt -p "(rename-window) " "rename-window '%%'"

bind-key -n F6 next-window \; display-panes
bind-key -n S-F6 next-layout \; display-panes

# Maybe put shift-f7 here for this if needed?
#bind-key -n F7 copy-mode

## Swap the defaults so that shift is by-window and none is by-pane
##bind-key -n S-F2 new-window -c "#{pane_current_path}" \; rename-window "-"
## For concentration - name windows when creating them
#bind-key -n S-F2 new-window -c "#{pane_current_path}" \; command-prompt -p "(rename-window) " "rename-window '%%'"
#bind-key -n F2 display-panes \; split-window -v -c "#{pane_current_path}"
#bind-key -n S-F3 previous-window
#bind-key -n S-F4 next-window
#bind-key -n F3 display-panes \; select-pane -t :.-
#bind-key -n F4 display-panes \; select-pane -t :.+
##bind-key -n S-F8 command-prompt -p "(rename-window) " "rename-window '%%'"
##bind-key -n F8 next-layout
