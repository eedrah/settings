-------- Setup --------
hs.alert.show("Hammerspoon loading...")
hs.loadSpoon("SpoonInstall")
spoon.SpoonInstall:andUse("ReloadConfiguration", {
    start = true
})

-------- Helpful variables and functions --------
local allKeys = (function() 
  local outKeys = {}
  for key, value in pairs(hs.keycodes.map) do
    if type(key) == "string" then
      outKeys[key] = value
    end
  end
  return outKeys
end)()
-- print(hs.inspect.inspect(allKeys))

-------- Modal setup --------
-- This requires Spotlight cmd-space shortcut to be turned off
local modal = hs.hotkey.modal.new("cmd", "space", 'modal')

-- Make all keys exit if they aren't overridden later
for key in pairs(allKeys) do
  modal:bind(nil, key, 'cancel', function() modal:exit() end)
  modal:bind('cmd', key, 'cancel', function() modal:exit() end)
end

local modalBindAndExit = function (key, message, fn)
  modal:bind(nil, key, message, function() modal:exit() fn() end)
  modal:bind('cmd', key, message, function() modal:exit() fn() end)
end

-------- Modal functions --------
-- spoon.SpoonInstall:andUse("WindowScreenLeftAndRight")
local function moveWindow(direction)
  local appWindow = hs.window.focusedWindow()
  if direction == "left" then
    screen = appWindow:moveOneScreenWest()
  elseif direction == "right" then
    appWindow:moveOneScreenEast()
  end
  appWindow:maximize()
end
modal:bind('shift', 'h', function() modal:exit() moveWindow('left') end)
modal:bind('shift', 'l', function() modal:exit() moveWindow('right') end)

spoon.SpoonInstall:andUse("MiroWindowsManager", {
    config = {
        hotkeyContext = modal
    },
    hotkeys = {
        up = {nil, 'k'},
        right = {nil, 'l'},
        down = {nil, 'j'},
        left = {nil, 'h'},
        fullscreen= {nil, 'f'}
    }
})

--[[
---- Jesse's edits to MiroWindowsManager
obj.hotkeyContext = hs.hotkey
local hotkeyContext = (function()
  local hkc = {}
  function hkc:bind(a,b,c,d,e,f)
    if obj.hotkeyContext == hs.hotkey then
      hs.hotkey.bind(a,b,c,d,e,f)
    else
      local pressedFn
      if d then
        pressedFn = function() obj.hotkeyContext:exit() d() end
      else
        pressedFn = function() obj.hotkeyContext:exit() end
      end
      obj.hotkeyContext:bind(a,b,c,pressedFn,e,f)
    end
  end
  return hkc
end)()
-- and "hs.hotkey -> hotkeyContext" in the spoon
---- end Jesse's edits
--]]

modalBindAndExit('d', 'displays', function() hs.osascript.applescript([[
  tell application "System Preferences"
    activate
    reveal anchor "displaysArrangementTab" of pane id "com.apple.preference.displays"
  end tell
]]) end)

modalBindAndExit('q', 'lock', function() hs.caffeinate.lockScreen() end)

-------- App switching --------

local appSwitcher = function(appName)
  -- appName can be found in the path to the application XXX.app or in cmd-tab
  -- This can be different to the window title
  local app = hs.application(appName)
  if not app then
    hs.application.launchOrFocus(appName)
  elseif app:isFrontmost() then 
    local appWindowSwitcher = hs.window.switcher.new { app:title() }
    appWindowSwitcher:previous()
  else
    app:setFrontmost()
  end
end

local bindSwitchApp = function(key, appName)
  modalBindAndExit(key, appName, function() appSwitcher(appName) end)
end

bindSwitchApp('m', 'Mission Control')
bindSwitchApp('b', 'Google Chrome')

bindSwitchApp('2', 'iTerm')
bindSwitchApp('3', 'Visual Studio Code')
bindSwitchApp('4', 'Microsoft Teams')
bindSwitchApp('5', 'Microsoft Outlook')
bindSwitchApp('6', 'zoom.us')
bindSwitchApp('7', 'Cypress')
bindSwitchApp('-', 'Hammerspoon')

-------- Browser switching --------
local mainBrowserFilter = hs.window.filter.new('Google Chrome'):setAppFilter('Google Chrome', {allowTitles = 'Google Chrome – bnz'})
local mainBrowserSwitcher = hs.window.switcher.new(mainBrowserFilter)
modalBindAndExit('1', 'Browser', function() mainBrowserSwitcher:previous() end)

modalBindAndExit('0', 'Personal browser', function()
  hs.window.find('Google Chrome – Personal'):focus()
end)

-------- Window arranger --------

function getScreens()
  local laptopScreen = hs.screen.find('Built%-in')

  local otherScreens = hs.fnutils.filter(hs.screen.allScreens(), function(screen)
    return screen ~= laptopScreen
  end)
  local codingScreen = otherScreens[1]
  local browserScreen = otherScreens[2] or laptopScreen

  return { laptop = laptopScreen, coding = codingScreen, browser = browserScreen }
end

function arrangeWindows()
  local maximized = {0, 0, 1, 1}

  local expectedWindows = {
    laptop = {
      hs.application'Outlook' and hs.application'Outlook':focusedWindow(),
      hs.application'Teams' and hs.application'Teams':focusedWindow(),
      hs.window.find('Google Chrome – Personal'),
    },
    coding = {
      hs.application'Code' and hs.application'Code':focusedWindow()
    }
  }
  expectedWindows.browser = hs.fnutils.filter(hs.window.filter.default:getWindows(), function(window)
    if hs.fnutils.contains(expectedWindows.laptop, window)
      or hs.fnutils.contains(expectedWindows.coding, window) then
        return false
      else
        return true
      end
  end)

  for screenName, screen in pairs(getScreens()) do
    for _, window in pairs(expectedWindows[screenName]) do
      if window then
        print('Moving window', window, ' to ', screenName, ' which is ', screen )
        window:move(maximized, screen)
      end
    end
  end
end
modalBindAndExit('a', 'arrange', arrangeWindows)

-------- Seal --------

spoon.SpoonInstall:andUse("Seal", {
    fn = function(s)
        s:loadPlugins({"apps", "calc", "useractions"})
    end,
    start = true,
})
spoon.Seal.plugins.useractions.actions = {
  ["Fix Big IP/RAS"] = {
    fn = function()
      hs.alert('fix-pac script running...')
      local bigIp = hs.application.get('BIG-IP Edge Client')
      if bigIp then
        bigIp:kill()
      end
      hs.execute('/usr/local/tools/fix-pac')
      hs.alert('fix-pac script run')
    end
  },
  ["Type Capex timesheet"] = {
      fn = function()
        hs.alert('started')
        hs.eventtap.keyStrokes('PCAC-2')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('6.8')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'return')
        hs.eventtap.keyStroke('cmd', 'return')
        hs.alert('done')
      end
  },
  ["Type Opex timesheet"] = {
      fn = function()
        hs.alert('started')
        hs.eventtap.keyStrokes('PCAC-13')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('.7')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'space', 1000000)
        hs.eventtap.keyStroke({}, 'return')
        hs.eventtap.keyStroke('cmd', 'return')
        hs.alert('done')
      end
  },
  ["Type Invoxy timesheet"] = {
      fn = function()
        hs.eventtap.keyStrokes('7.5')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('7.5')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('7.5')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('7.5')
        hs.eventtap.keyStroke({}, 'tab')
        hs.eventtap.keyStrokes('7.5')
        hs.eventtap.keyStroke({}, 'tab')
      end
  },
  ['Type date'] = {
    fn = function()
      hs.eventtap.keyStrokes(os.date("%F"))
    end
  },
}
modal:bind('cmd', 'space', 'Seal', function() spoon.Seal:show() modal:exit() end)

-------- Other Spoons --------

spoon.SpoonInstall:andUse("Caffeine", {
  start = true
})

spoon.SpoonInstall:andUse("Cherry")

-------- Works in progress --------

local noiseListener = hs.noises.new(function(event)
  if event == 3 then
    hs.eventtap.keyStroke({}, 'right')
  end
end)
local noiseListenerMenu = hs.menubar.new()
function toggleNoiseListener()
  if noiseListenerMenu:title() == 'Off' then
    noiseListenerMenu:setTitle('On...')
      hs.alert('started')
      noiseListener:start()
  else
    noiseListenerMenu:setTitle('Off')
      hs.alert('stopped')
      noiseListener:stop()
  end
end
noiseListenerMenu:setClickCallback(toggleNoiseListener)
toggleNoiseListener()

-------- Leftover config --------

-- also look at these for ideas:
-- https://gist.github.com/lucifr/b0780e38045235027ef11746041dc120
-- https://github.com/cmsj/hammerspoon-config/blob/master/init.lua

-- more pages related to hotkeys and launching apps - haven't figured them out yet
-- https://www.hammerspoon.org/Spoons/AppLauncher.html
-- https://www.hammerspoon.org/docs/hs.hotkey.modal.html
-- https://www.hammerspoon.org/Spoons/ModalMgr.html
-- https://www.hammerspoon.org/Spoons/RecursiveBinder.html

-- https://www.hammerspoon.org/Spoons/PushToTalk.html
-- or also
-- https://www.hammerspoon.org/Spoons/MicMute.html

-- also need to clear out Spoons folder with only those needed - or hopefully the SpoonInstall thing would work, so don't need to commit them to yadm

-- TODO
-- create hotkey to split screen current window and last window?

-- TOOLS AVAILABLE
-- applescript
-- typing/accessibility function
-- pasteboard / pasting
-- layout function
-- hammerspoon urls - hammerspoon://someAlert

hs.alert.show("...finished")