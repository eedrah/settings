#! /bin/bash
# shellcheck disable=SC1090 disable=SC1091

BASHRC_STARTED_AT=$(date +%s)

################ Starting scripts ################
# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return ;;
esac

# Determine environment
if [[ $DETERMINED_ENVIRONMENT ]]; then
	printf %b "$DETERMINED_ENVIRONMENT"
elif [ -f "$HOME/bin/ee-determine-environment.sh" ]; then
	source "$HOME/bin/ee-determine-environment.sh"
	printf %b "$DETERMINED_ENVIRONMENT"
else
	echo "Did not determine environment"
fi
[[ -z $IS_NOT_GUEST_COMPUTER ]] && echo "On a guest computer"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ]; then
	PATH="$HOME/bin:$PATH"
fi
if [ -d "$HOME/bin.local" ]; then
	PATH="$HOME/bin.local:$PATH"
fi

################ Bash options ################
# shellcheck disable=SC2071
if [[ "$BASH_VERSION" > 3 ]]; then
	set -o vi
	bind -m vi-insert '"jk": ""'

	# Tab completion
	bind 'TAB: complete'
	bind 'set show-all-if-ambiguous on'
	bind 'set completion-ignore-case on'
	# Search history with already typed text
	bind '"\e[A": history-search-backward'
	bind '"\e[B": history-search-forward'
	bind '"\C-p":previous-history'
	bind '"\C-n":next-history'

	shopt -s cdspell
	shopt -s checkwinsize
	shopt -s cmdhist
	shopt -s dotglob # includes .dotfiles in glob
	shopt -s expand_aliases
	shopt -s extglob

	shopt -s histappend
	HISTCONTROL=ignoreboth # don't put duplicate lines or lines starting with space in the history.
	HISTSIZE=10000
	HISTFILESIZE=20000

	shopt -s histreedit
	shopt -s histverify
	shopt -s hostcomplete
	shopt -s interactive_comments
	shopt -s lithist
	shopt -s no_empty_cmd_completion
	shopt -u nocaseglob
	shopt -u nocasematch
	shopt -u nullglob
	shopt -s progcomp
	shopt -s promptvars
	shopt -s sourcepath

	stty -ixon # allow ctrl-s to search forward
fi
# shellcheck disable=SC2071
if [[ "$BASH_VERSION" > 4 ]]; then
	# Tab completion
	bind '"\C-i": menu-complete'
	#bind 'TAB: menu-complete'
	bind '"\e[Z": menu-complete-backward'
	bind 'set menu-complete-display-prefix on'

	bind 'set enable-bracketed-paste on'

	# mappings for Ctrl-left-arrow and Ctrl-right-arrow for word moving
	# ^[ is the same as \e
	bind '"\e[1;5C": forward-word'
	bind '"\e[1;5D": backward-word'
	bind '"\e[5C": forward-word'
	bind '"\e[5D": backward-word'
	bind '"\e\e[C": forward-word'
	bind '"\e\e[D": backward-word'

	shopt -s globstar
fi

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color | *-256color) color_prompt=yes ;;
esac

force_color_prompt=yes
if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
		# We have color support; assume it's compliant with Ecma-48
		# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
		# a case would tend to support setf rather than setaf.)
		color_prompt=yes
	else
		color_prompt=
	fi
fi

if [ "$color_prompt" = yes ]; then
	PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\D{%F %T} \u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
	xterm* | rxvt*)
		# shellcheck disable=SC1117
		PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
		;;
	*) ;;

esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	# shellcheck disable=SC2015
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

################ Bash completion ################
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	elif [[ -n $IS_MAC ]] && [ -f /usr/local/etc/bash_completion ]; then
		. /usr/local/etc/bash_completion
	fi
fi

. "$HOME/bin/ee-load-nvm.sh"

################ Environment variables ################
export VISUAL=vim
#export ANDROID_SDK_ROOT="/usr/local/share/android-sdk"
#export ANDROID_SDK_ROOT="$HOME/Library/Android/sdk"
#export PATH="$PATH:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools"
export GPG_TTY="$(tty)" # https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html#Invoking-GPG_002dAGENT

export GDRIVE_CONFIG_DIR="$HOME/.secrets/google-drive"

# python setup
export PIPENV_VENV_IN_PROJECT=1 # keep .venv in each local folder like node_modules, not global
export MPLBACKEND=TkAgg # Prevent matplotlib crashing on OSX
export PIPENV_MAX_DEPTH=5
export BASH_SILENCE_DEPRECATION_WARNING=1

################ Prompt and colors ################
. ee-activate-git-prompt.sh

################ Bash aliases and functions ################
# Disables
alias which='printf %s\\n "Not available - use type -a"'

# use s as an alias instead of l, as on other systems l is common but I won't know what it does
alias ls='echo "Not available - use s and sl"'
alias s='/bin/ls -Av'
alias sl='/bin/ls -aFhlv'
if [[ -n $IS_MAC ]]; then
	alias s='/bin/ls -Av -G'
	alias sl='/bin/ls -aFhlv -G'
elif [[ -n $IS_LINUX ]]; then
	alias s='/bin/ls -Av --color=auto'
	alias sl='/bin/ls -aFhlv --color=auto'
	#alias sl='/bin/ls -aFhlv --time-style=iso --color=auto'
fi

# Overrides
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

#### One basic thing
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
alias ........='cd ../../../../../../..'
alias .........='cd ../../../../../../../..'

alias cpa='rsync -ah --progress'
alias vless='/usr/share/vim/vim80/macros/less.sh'
#alias space='du -sh {.??,}*'
alias space='du -sh . * | sort -rh'
alias datetime='date -u +"%Y-%m-%dT%H:%M:%SZ"'

alias pyenv='pipenv run python'
alias pyvim='pipenv run vim'

#### Two basic things at once
c() { cd "$1" && s; }
mkcd() { mkdir "$1" && cd "$1"; }
cdln() { cd "$(dirname "$(dirname "$1")/$(readlink "$1")")"; }
lns() {
	if [ -d "$2" ]; then
		folder="$2"
	else
		folder="$(dirname "$2")"
	fi
	ln -s "$(realpath --relative-to "$folder" "$1")" "$2"
}
mvln() {
	if [ -d "$2" ]; then
		mv "$1" "$2"
		lns "$2/$(basename "$1")" "$1"
	else
		mv "$1" "$2"
		lns "$2" "$1"
	fi
}
mkscript() { touch "$1" && chmod +x "$1" && printf '#!/bin/bash\nset -eo pipefail\n' >>"$1" && vim "$1"; }

# Highly specific things in order of how common they are used
last-command() {
	history $((1 + ${1:-1})) | awk 'NR==1{$1=""; print substr($0,2)}'
}
alias save-last-command='last-command >> ~/saved-commands && echo "Saved the following command:" && tail -n 1 ~/saved-commands'

alias vim-install='vim -c ":PlugClean! | :PlugInstall | :qall"'
alias webshare='python -c "import SimpleHTTPServer; SimpleHTTPServer.test()"'

node-watch() {
	watch -d -n 3 "inotifywait -q --format '%w' \"$1\" | xargs -I{} node {}"
}
dd-iso() {
	sudo dd bs=1m if="$1" | pv -s "$(stat -f %z "$1")" | sudo dd bs=1m of="$2"
}
lyrics() {
	w3m "google.com/search?q=lyrics $*" | less
}
qr-code() {
	curl qrenco.de/"$1"
	#echo STRING | curl -F-=\<- qrenco.de
}
#curl -s http://tinyurl.com/api-create.php?url=http://www.google.com
# text sharing, see: https://github.com/chubin/awesome-console-services

alias pomodoro='nohup xwrits typetime=25 breaktime=5 +mouse +clock +breakclock title="Wrists, Water, Eyes, Stand, Yoga" > /tmp/xwrits_output & disown'
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '"'"'s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'"'"')"' # Usage: sleep 10; alert
alias psd-stop='systemctl --user stop psd'
alias psd-start='systemctl --user start psd'
alias reconfigure-openbox='openbox --config-file ~/.config/openbox/lubuntu-rc.xml --reconfigure;'
alias tclip='tee >(clip)'
# could also put tr -d '\n' in front to get rid of trailing newline
alias weather='curl wttr.in/Wellington'
alias joke='curl https://icanhazdadjoke.com'
alias prettier='npx prettier --write'

#### Augment personal scripts
#alias ee-continue='ee-setup ; [ -e ~/continue.sh ] && . ~/continue.sh'
alias ee-continue='[ -e ~/continue.sh ] && . ~/continue.sh'
alias cont='ee-continue'
alias setup='ee-continue'
alias ee-sshagent='. ee-sshagent.sh'

#### Git
alias st='git status -s -b'
# shellcheck disable=SC2015
clone() { git clone "https://github.com/eedrah/$1" && cd "$1" && git remote set-url --push origin "git@github.com:eedrah/$1" || cd "$1"; }
# Edited from
# https://www.quora.com/How-can-I-make-a-git-alias-that-executes-a-cd
# derived from http://superuser.com/questions/105375/bash-spaces-in-alias-name
function git() {
	case $1 in
		init)
			shift
			command git initialize "$@" && cd "$1"
			;;
		cd) cd="$(git rev-parse --show-toplevel)" && command cd "$cd" ;;
		pussh)
			shift
			source ee-sshagent.sh && command git push "$@"
			;;
		*) command git "$@" ;;
	esac
}
alias g="__git_complete g __git_main; alias g=\"git\" && git"
__git_complete git-repos __git_main
alias git-repos='git for-each-subdirectory'
alias g-repos='git for-each-subdirectory'

#### Docker
alias alpine='docker run --rm -it alpine'
alias ubuntu='docker run --rm -it ubuntu'
# docker working directory
alias dwd='docker run --rm -it -v "$PWD":/app -w /app'
dbash() {
	docker run --rm -it -p 5500:5500 "$@" bash /usr/bin/env bash -c "apk update && apk add ca-certificates wget && wget -qO- https://eedrah.com/settings/bootstrap | bash | tail -n 1 > /tmp/bootstrap && source /tmp/bootstrap"
}

function docker() {
	case $1 in
		ls | list)
			command docker network list
			echo
			command docker volume list
			echo
			command docker image list
			echo
			command docker ps -a
			;;
		c | compose)
			shift
			docker-compose "$@"
			;;
		#vim ) shift; docker run --rm -it "$@" thinca/vim vim ;;
		vim)
			shift
			TEMPFILE=$(mktemp) && docker cp "$1:$2" "$TEMPFILE" && vim "$TEMPFILE" && docker cp "$TEMPFILE" "$1:$2"
			;;

		pull)
			case $2 in
				--all | all) command docker images | grep -v REPOSITORY | awk '{print $1 ":" $2}' | xargs -L1 command 'docker' pull ;;
				*) command docker "$@" ;;
			esac
			;;
		image)
			case $2 in
				--clean | clean) command docker images | grep '<none>' | awk '{print $3}' | xargs 'docker' image rm ;;
				*) command docker "$@" ;;
			esac
			;;
		container)
			case $2 in
				--delete-all) command docker container list --all | awk 'NR > 1 {print $1}' | xargs 'docker' container rm -f ;;
				*) command docker "$@" ;;
			esac
			;;
		volume)
			case $2 in
				--delete-all) command docker volume list | awk 'NR > 1 {print $2}' | xargs 'docker' volume rm -f ;;
				*) command docker "$@" ;;
			esac
			;;
		*) command docker "$@" ;;
	esac
}
alias d=docker

function oc() {
	case $1 in
		token)
			chrome --profile nzp https://oauth-openshift.apps.cluster-1.stdt-aws.nzpdigital.co.nz/oauth/token/request
			;;
		first-pod)
			echo "Changed to oc pod first"
			;;
		app-pod)
			echo "Changed to oc pod app"
			;;
		pod)
			case $2 in
				app)
					oc get pod --selector app=$3 -o name
					;;
				run)
					oc get pod --selector run=$3 -o name
					;;
				live)
					oc get pod --field-selector status.phase=Running -o name
					;;
				first)
					oc pod live | head -1
					;;
				*)
					echo "Please specify: app|run|live|first"
					exit 100 ;;
			esac
			;;
		service-forward)
			shift
			oc port-forward service/$1 $(oc get service "$1" -o jsonpath='{.spec.ports[0].targetPort}')
			;;
		vim)
			shift
			TEMPFILE=$(mktemp -d) && oc cp "$1:$2" "$TEMPFILE" && vim "$TEMPFILE/$(basename "$2")" && oc cp "$TEMPFILE/$(basename "$2")" "$1":"$2"
			;;
		*) command oc "$@" ;;
	esac
}

GDRIVE_PWD_ID=root
GDRIVE_PWD_PATH=/
GDRIVE_PWD_PARENT=/
function gdrive() {
	case $* in
		search*)
			shift
			gdrive list --query "name='$*'"
			;;
		cd*)
			shift
			[[ $1 == -q ]] && local quiet=true && shift
			case $* in
				'' | /)
					GDRIVE_PWD_ID=root
					GDRIVE_PWD_PATH=/
					GDRIVE_PWD_PARENT=/
					;;
				*/*)
					IFS=/ read -ra DIRS <<<"$@"
					for dir in "${DIRS[@]}"; do
						gdrive cd -q "$dir"
					done
					;;
				..)
					gdrive set-pwd-from-info "$(gdrive info "$GDRIVE_PWD_PARENT")"
					;;
				-i* | --id*)
					shift
					gdrive set-pwd-from-info "$(gdrive info "$1")"
					;;
				*)
					gdrive cd -q --id "$(gdrive get-id-from-name "$*")"
					;;
			esac
			[[ -z $quiet ]] && gdrive pwd
			;;
		pwd*) echo "$GDRIVE_PWD_PATH" ;;
		ls*) gdrive list --name-width 0 --order name_natural --max 100 --query "'$GDRIVE_PWD_ID' in parents and trashed = false" ;;
		cp*)
			shift
			gdrive upload --parent "$GDRIVE_PWD_ID" "$@"
			;;
		get-id-from-name*)
			shift
			gdrive list --query "name='$*' and '$GDRIVE_PWD_ID' in parents and trashed = false" | head -2 | tail -1 | cut -f1 -d' '
			;;
		set-pwd-from-info*)
			shift
			GDRIVE_PWD_PARENT=/
			while read -r key value; do
				[[ "$key" == 'Id:' ]] && GDRIVE_PWD_ID="$value"
				[[ "$key" == 'Path:' ]] && GDRIVE_PWD_PATH="$value"
				[[ "$key" == 'Parents:' ]] && GDRIVE_PWD_PARENT="$value"
			done <<<"$@"
			;;
		mkdir*)
			shift
			command gdrive mkdir -p "$GDRIVE_PWD_ID" "$@"
			;;
		sync-up*)
			shift
			command gdrive sync upload "$@" . "$GDRIVE_PWD_ID"
			;;
		sync-down*)
			shift
			command gdrive sync download "$@" "$GDRIVE_PWD_ID" .
			;;
		*) command gdrive "$@" ;;
	esac
}

echo Finished init in $(($(date +%s) - $BASHRC_STARTED_AT)) seconds, running tests...

################ Find ssh-agent if it's running ################
. ee-sshagent.sh --find
# removed because it takes a lot of time

################ Print daily tests ################
if [ "$IS_NOT_GUEST_COMPUTER" ]; then
	#### Local
	LAST_RUN_LOCAL="$HOME/git/tests/last-run.local"
	if [ -f "$LAST_RUN_LOCAL" ] && [ "$(head -1 "$LAST_RUN_LOCAL")" = "$(date +%Y-%m-%d)" ] && [ $(tail -1 "$LAST_RUN_LOCAL") = 0 ]; then
		echo Local tests passed.
	else
		echo Local tests failed.
		tail +2 "$LAST_RUN_LOCAL" | sed '$d'
	fi
	unset LAST_RUN_LOCAL

	#if [ "$(cat "$HOME/git/tests/date-last-run")" != "$(date +%Y-%m-%d)" ]; then
		#"$HOME/git/tests/test"
	#fi

	#### Remote
	LAST_RUN_REMOTE=$(mktemp)
	if ! nc -G 2 $(<"$HOME/.secrets/env/FAIRY_IP") $(<"$HOME/.secrets/env/FAIRY_PORT") < /dev/null > /dev/null; then
		echo Could not connect to remote tests.
	else
		scp -P $(<"$HOME/.secrets/env/FAIRY_PORT") fairy@$(<"$HOME/.secrets/env/FAIRY_IP"):/home/fairy/git/tests/last-run.remote $LAST_RUN_REMOTE >/dev/null
		if [ $? = 0 ] && [ -f "$LAST_RUN_REMOTE" ] && [ "$(head -1 "$LAST_RUN_REMOTE")" = "$(date -u +%Y-%m-%d)" ] && [ "$(tail -1 "$LAST_RUN_REMOTE")" = 0 ]; then
			echo Remote tests passed.
		else
			echo Remote tests failed.
			tail +2 "$LAST_RUN_REMOTE" | sed '$d'
		fi
	fi
fi

echo ".bashrc run in $(($(date +%s) - $BASHRC_STARTED_AT)) seconds"
