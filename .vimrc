" ======================== Useful info ========================

" C-V - Escape the next character
" i C-O - Do one command in insert mode
" i C-\ C-O like C-O but don't move the cursor

" C-I = Tab
" C-[ = Esc
" C-M = Enter
" C-H = Backspace

":help index
" will tell you all default key _bindings_ for all modes, in the absence
" of _mappings_. To see the latter, and where they were set, use both
":verbose map
":verbose map!
" (with ! for Insert/Replace and Command-line modes, without it for
" Normal, Visual and Operator-Pending), see
":help map-listing
":help :map-verbose
":help :verbose
":help :verbose-cmd
" See also
":help map-which-keys
" about what are the best keys for a {lhs}

" This is the definition of * and #, and is useful to know the <C-R>= part
":nnoremap * /\<<C-R>=expand('<cword>')<CR>\><CR>
":nnoremap # ?\<<C-R>=expand('<cword>')<CR>\><CR>

" ======================== Available keys ========================
" i.e. Functions that I don't like

" C-U - line undo

" ======================== Unused ========================
" To show info about last normal command
"echo v:count v:operator v:event @.

"https://stackoverflow.com/questions/3760444/in-vim-is-there-a-way-to-set-very-magic-permanently-and-globally/23021259#23021259

" Also investigate the following vimrcs:
"https://github.com/amix/vimrc

" To source this while editing, try:
" :so %

" save session
"nnoremap <leader>s :mksession<CR>
"Ever wanted to save a given assortment of windows so that they're there next time you open up Vim? :mksession does just that! After saving a Vim session, you can reopen it with vim -S. Here I've mapped it to ,s, which I remember by thinking of it as "super save".

" open ag.vim
"nnoremap <leader>a :Ag
"The Silver Searcher is a fantastic command line tool to search source code in a project. It's wicked fast. The command line tool is named ag (like the element silver). Thankfully there is a wonderful Vim plugin ag.vim which lets you use ag without leaving Vim and pulls the results into a quickfix window for easily jumping to the matches. Here I've mapped it to ,a.

"CtrlP
"ctrlp.vim is my life in Vim. If you've never used a fuzzy file searcher this will open your eyes. If you're currently using commandt.vim, you're on the right track, but CtrlP is the spiritual successor. It's can be (see below) significantly faster and more configurable than CommandT (Thanks Reddit!). Anyways here are my settings for CtrlP.
"
"" CtrlP settings
"let g:ctrlp_match_window = 'bottom,order:ttb'
"let g:ctrlp_switch_buffer = 0
"let g:ctrlp_working_path_mode = 0
"let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
"There are a few things happening here. The first is I'm telling CtrlP to order matching files top to bottom with ttb. Next, we tell CtrlP to always open files in new buffers with let ctrlp_switch_buffer=0. Setting let g:ctrlp_working_path=0 lets us change the working directory during a Vim session and make CtrlP respect that change.
"
"Now, let's talk about speed. CtrlP is entirely written in Vimscript, (which is pretty impressive) but CommandT has parts that are written in C. This means CommandT is, by default, faster than CtrlP. However, we can tell CtrlP to run an external command to find matching files. Now that we have ag installed, we can use it with CtrlP to make CtrlP wicked fast.. We do that with the following.
"
"let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
"If everything works out, you should see a noticeable improvement in the CtrlP speed. There are two caveats to this. Both g:ctrlp_show_hidden and g:ctrlp_custom_ignore do not work with custom user commands. I only care about the lack of support for custom ignores. Thankfully, ag has it's own convention for ignore files: a .agignore file that follows the same conventions as .gitignore. This is actually great! We only need to define our directories to ignore when searching in one place.

"Tmux
"" allows cursor change in tmux mode
"if exists('$TMUX')
    "let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
    "let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
"else
    "let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    "let &t_EI = "\<Esc>]50;CursorShape=0\x7"
"endif
"These lines change the cursor from block cursor mode to vertical bar cursor mode when using tmux. Without these lines, tmux always uses block cursor mode.

"Backups
"If you leave a Vim process open in which you've changed file, Vim creates a "backup" file. Then, when you open the file from a different Vim session, Vim knows to complain at you for trying to edit a file that is already being edited. The "backup" file is created by appending a ~ to the end of the file in the current directory. This can get quite annoying when browsing around a directory, so I applied the following settings to move backups to the /tmp folder.
"set backup
"set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
"set backupskip=/tmp/*,/private/tmp/*
"set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
"set writebackup
"backup and writebackup enable backup support. As annoying as this can be, it is much better than losing tons of work in an edited-but-not-written file.

" ======================== Opening config ========================

source $VIMRUNTIME/vimrc_example.vim
let &runtimepath = $SSHHOME."/.sshrc.d/.vim,".&runtimepath

let mapleader=","
set nocompatible

" ======================== Vim options ========================

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on

syntax on " Enable syntax highlighting

set hidden " Allows background hidden buffers
set showcmd " Show partial commands in the last line of the screen

" Modelines have historically been a source of security vulnerabilities. As
" such, it may be a good idea to disable them and use the securemodelines
" script, <http://www.vim.org/scripts/script.php?script_id=1876>.
set nomodeline

set ignorecase " Use case insensitive search
set smartcase " except when using capital letters

set backspace=indent,eol,start " Allow backspacing over autoindent, line breaks and start of insert action

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Always display the status line, even if only one window is displayed
set laststatus=2

" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm

" Use visual bell instead of beeping when doing something wrong
set visualbell
" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
"set t_vb=

" Enable use of the mouse for all modes
set mouse=a

" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2

" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200
" Actually, do time out on mappings
set timeout

" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab

" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
set tabstop=4

set number              " show line numbers
set relativenumber
set cursorline          " highlight current line
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]

set incsearch           " search as characters are entered
set hlsearch            " highlight matches
nohlsearch              " but don't hightlight when sourcing

set nofoldenable          " enable/disable folding at start
"set foldlevelstart=10   " open most folds by default at start
set foldnestmax=30      " only fold first X number of folds
set foldmethod=indent   " fold based on indent level

"set backupdir=~/.vim/backup//
"set directory=~/.vim/swap//
"set undodir=~/.vim/undo//
set nobackup
set noswapfile
set noundofile

"set tildeop " Make ~ an operator
set clipboard=unnamed " Use system clipboard as main clipboard
set switchbuf=useopen

set cryptmethod=blowfish2
" Can use vim -x filename.txt to encrypt files. But confirm strength of
" security before using for anything important

set wildmode=longest,list,full
set splitbelow
set splitright

set encoding=utf-8

set iskeyword+=- " Treats words seperated by dashes as one word

" -------- Completion --------
set omnifunc=syntaxcomplete#Complete

" longest - only insert longest common text of matches
" menuone - show menu even when there is only one match
"set completeopt=preview,menuone " Default
set completeopt=longest,menuone

" ======================== Plugins ========================

if !isdirectory($VIM_PLUGINS_DIR)
    call mkdir($VIM_PLUGINS_DIR)
endif
call plug#begin($VIM_PLUGINS_DIR)
"" On-demand loading
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
"
"" Using a non-master branch
"Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
"
"" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"Plug 'fatih/vim-go', { 'tag': '*' }
"
"" Plugin options
"Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
"
"" Plugin outside ~/.vim/plugged with post-update hook
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"
"" Unmanaged plugin (manually installed and updated)
"Plug '~/my-prototype-plugin'

" -------- Appearance --------
Plug 'altercation/vim-colors-solarized'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'vim-airline/vim-airline'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'

" -------- Automatic functionality --------
Plug 'bogado/file-line' " Can open files with file.txt:10 for line 10. Used in my git aliases
Plug 'DataWraith/auto_mkdir' " mkdir when it doesn't exist on save
Plug 'tpope/vim-repeat' " add ability to repeat plugin commands
"Plug 'tpope/vim-speeddating' " use CTRL-A/CTRL-X to increment dates, times, and more
Plug 'raimondi/delimitmate' " auto-complete brackets
Plug 'junegunn/vim-peekaboo' " show registers after delay of register selection
"Plug 'junegunn/rainbow_parentheses.vim'
"Plug 'styled-components/vim-styled-components'
"Plug 'hail2u/vim-css3-syntax'

" -------- Commands --------
"Plug 'takac/vim-hardtime' " Put a limit on using hjkl motions
Plug 'ap/vim-you-keep-using-that-word' " Make cw include trailing space
Plug 'tpope/vim-abolish' " Abolish (smart autoreplace/abreviations), Subvert (find/replace with cases and variants), Coerce to case cr_ (m:MiXed, c:caMel, s:sn_ake, u:UP_PER, -:da-sh, .:do.t, <space>:sp ace, t:Ti Tle)
Plug 'tpope/vim-surround' " s as an motion - s' s( st (for xml) s<a class=fish>, S in visual mode, y for selection mode - eg yss} or ysiw(
Plug 'tpope/vim-unimpaired' "[e,  ]<space>, =p/P - paste after/before linewise, [x ]u - xml/url en/decode [l - move in list
Plug 'svermeulen/vim-easyclip' " m is move, s is paste motion; makes d not yank
Plug 'scrooloose/nerdcommenter' " <leader>c_ to comment: (c)omment, (n)est, <space> toggle, (m)inimal, (i)nvert individual lines, (s)exy, (y)ank before c, ($) end of line, (A)ppend comment, (a)lternate delimiter, align on (l)eft or (b)oth sides, (u)ncomment
Plug 'haya14busa/vim-asterisk' " z* doesn't move cursor, * uses smart case, * keeps cursor position
Plug 'kana/vim-textobj-user' " used for beloglazov/vim-textobj-quotes
Plug 'beloglazov/vim-textobj-quotes' " use q and iq as a motion for inside all types of quotes
Plug 'bkad/CamelCaseMotion' " use <leader>w and similar to move within camelCased words
Plug 'inkarkat/argtextobj.vim' " use a as 'argument' motion inside function signatures
Plug 'michaeljsmith/vim-indent-object' " use ii and ai as 'indentation' motions - ai includes line above and below
Plug 'jeetsukumaran/vim-indentwise' " use [ or ]- ]+ ]= (move to relative indent), <count>]_ (move to absolute indent), ]% (move to begin/end of indent block) as indentation motions

" -------- Adding to vim --------
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'w0rp/ale'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mbbill/undotree'
Plug 'chamindra/marvim'

" -------- Completion --------
Plug 'mattn/emmet-vim' "prefix: <C-Y> - ,:expand ;:word u:update d:selectIn D:selectOut n:nextEdit i:imageDimensions k:remove j:join/split  - {}:text *:multiple $:numberingAndPadding $#:insertTextAtLocation @:numberStart/direction:-
Plug 'Valloric/YouCompleteMe', { 'do': 'python3 ./install.py' }
Plug 'SirVer/ultisnips' " <c-j> and <c-k> to go forward and back in a snippet
Plug 'honza/vim-snippets'
Plug 'epilande/vim-es2015-snippets'
Plug 'epilande/vim-react-snippets'

call plug#end()

" ======================== Plugins remapping function ========================
" https://stackoverflow.com/questions/24027506/get-a-vim-scripts-snr
func! GetScriptNumber(script_name)
    " Return the <SNR> of a script.
    "
    " Args:
    "   script_name : (str) The name of a sourced script.
    "
    " Return:
    "   (int) The <SNR> of the script; if the script isn't found, -1.

    redir => scriptnames
    silent! scriptnames
    redir END

    for script in split(l:scriptnames, "\n")
        if l:script =~ a:script_name
            return str2nr(split(l:script, ":")[0])
        endif
    endfor

    return -1
endfunc

" ======================== Plugins Options ========================

"Plug 'altercation/vim-colors-solarized'
set background=dark
silent! colorscheme solarized
highlight SignColumn ctermbg=black

"Plug 'nathanaelkane/vim-indent-guides'
let g:indent_guides_enable_on_vim_startup = 1
let indent_guides_auto_colors = 0
let indent_guides_guide_size = 1
hi IndentGuidesOdd ctermbg=234
hi IndentGuidesEven ctermbg=235

"Plug 'vim-airline/vim-airline'
let g:airline#extensions#tabline#enabled = 1

"Plug 'mxw/vim-jsx'
let g:jsx_ext_required = 0

"Plug 'tpope/vim-speeddating' " use CTRL-A/CTRL-X to increment dates, times, and more
"augroup speeddating
"    autocmd!
"    autocmd VimEnter * SpeedDatingFormat! %v | SpeedDatingFormat! %^v
"augroup END

"Plug 'junegunn/vim-peekaboo'
let g:peekaboo_delay = 1000
"let g:peekaboo_compact = 1

"Plug 'raimondi/delimitmate'
let g:delimitMate_expand_cr = 2
let g:delimitMate_expand_space = 1
let g:delimitMate_balance_matchpairs = 1
let g:delimitMate_jump_expansion= 1

"Plug 'junegunn/rainbow_parentheses.vim'
"let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]
"Custom pairs breaks jsx template literal syntax highlighting
"augroup rainbow_parentheses
"    autocmd!
"    autocmd VimEnter * RainbowParentheses
"augroup END

""Plug 'takac/vim-hardtime' " Put a limit on using hjkl motions
"" Commands :HardTimeToggle :HardTimeOn :HardTimeOff
"let g:hardtime_default_on = 1
""let g:hardtime_showmsg = 1
"let g:hardtime_allow_different_key = 1
"let g:hardtime_maxcount = 2
"let g:list_of_normal_keys = ["h", "j", "k", "l", "-", "+"]
"let g:list_of_visual_keys = ["h", "j", "k", "l", "-", "+"]
"let g:list_of_insert_keys = []

"Plug 'svermeulen/vim-easyclip'
nnoremap gm m
let g:EasyClipUseSubstituteDefaults = 1
let g:EasyClipAutoFormat = 1
nnoremap <leader>cf <plug>EasyClipToggleFormattedPaste
let g:EasyClipShareYanks = 1
let g:EasyClipShareYanksDirectory = '/tmp/'
nmap M <Plug>MoveMotionEndOfLinePlug

"Plug 'scrooloose/nerdcommenter'
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1

"Plug 'haya14busa/vim-asterisk'
map *   <Plug>(asterisk-*)
map #   <Plug>(asterisk-#)
map g*  <Plug>(asterisk-g*)
map g#  <Plug>(asterisk-g#)
map z*  <Plug>(asterisk-z*)
map gz* <Plug>(asterisk-gz*)
map z#  <Plug>(asterisk-z#)
map gz# <Plug>(asterisk-gz#)
"If you want to set "z" (stay) behavior as default
"map *  <Plug>(asterisk-z*)
"map #  <Plug>(asterisk-z#)
"map g* <Plug>(asterisk-gz*)
"map g# <Plug>(asterisk-gz#)
"To enable keepCursor feature:
let g:asterisk#keeppos = 1

"Plug 'beloglazov/vim-textobj-quotes'
xmap q iq
omap q iq

"Plug 'bkad/CamelCaseMotion'
let g:camelcasemotion_key = '<leader>'

"Plug 'scrooloose/nerdtree'
let NERDTreeShowHidden=1
"command! T NERDTreeToggle
nnoremap <leader>t :NERDTreeToggle<CR>

"Plug 'w0rp/ale'
let g:ale_sign_error = 'X' " could use emoji
let g:ale_sign_warning = '?' " could use emoji
let g:ale_sign_column_always = 1
let g:ale_statusline_format = ['X %d', '? %d', '']
let g:ale_echo_msg_format = '%linter% says %s'
let g:airline#extensions#ale#enabled = 1
let g:ale_fix_on_save = 1
let g:ale_fixers = {
    \'javascript': ['prettier'],
    \'typescript': ['prettier'],
    \'typescriptreact': ['prettier'],
    \'json': ['prettier'],
    \'yaml': ['prettier'],
    \'graphql': ['prettier'],
    \'css': ['prettier'],
    \'scss': ['prettier'],
    \'less': ['prettier'],
    \'sh': ['shfmt'],
    \'python': ['black'],
\}
"let g:ale_javascript_prettier_options = '--single-quote --no-semi --trailing-comma es5'

" Never got it working
"let g:ale_javascript_eslint_executable = 'eslint_d --cache'
"let g:ale_typescript_eslint_executable = 'eslint_d --cache'
"let g:ale_typescriptreact_eslint_executable = 'eslint_d --cache'

" Fix for slow NZTE project
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
"let g:ale_lint_on_enter = 0
"let g:ale_lint_on_save = 0

let g:ale_sh_shfmt_options = '-ci'
"let g:ale_open_list = 1
"augroup FiletypeGroup
"    autocmd!
"    au BufNewFile,BufRead *.jsx set filetype=javascript.jsx
"augroup END
" Limit linters used for JavaScript.
"let g:ale_linters = {
"    \  'javascript': ['flow', 'eslint']
"    \}
"highlight clear ALEErrorSign " otherwise uses error bg color (typically red)
"highlight clear ALEWarningSign " otherwise uses error bg color (typically red)
" Map keys to navigate between lines with errors and warnings.
nnoremap <leader>an :ALENextWrap<cr>
nnoremap <leader>ap :ALEPreviousWrap<cr>

"Plug 'ctrlpvim/ctrlp.vim'
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_show_hidden = 1
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard'] " Ignore .gitignore files

"Plug 'mbbill/undotree'
nnoremap <leader>u :UndotreeToggle<CR>
let g:undotree_WindowLayout = 2
let g:undotree_ShortIndicators = 1
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_HelpLine = 0
function! g:Undotree_CustomMap()
    nmap <buffer> k <plug>UndotreeGoNextState
    nmap <buffer> j <plug>UndotreeGoPreviousState
endfunc

"Plug 'vim-scripts/marvim'
let g:marvim_register = 'w'
let g:marvim_find_key = 'q2'
let g:marvim_store_key = 'q1'
let g:marvim_store = $HOME."/.vim/marvim"

"Plug 'mattn/emmet-vim'
"let g:user_emmet_leader_key='<Tab>'
let g:user_emmet_settings = {
\   'javascript.jsx' : {
\       'extends' : 'jsx',
\   },
\}

"Plug 'Valloric/YouCompleteMe'
let g:ycm_filepath_blacklist = {}
"let g:ycm_path_to_python_interpreter = '/usr/bin/python' "Disabled when it
"started spazing out

" Point YCM to the Pipenv created virtualenv, if possible
let pipenv_venv_path = system('pipenv --venv')
if shell_error == 0
    let venv_path = substitute(pipenv_venv_path, '\n', '', '')
    let g:ycm_python_binary_path = venv_path . '/bin/python'
else
    let g:ycm_python_binary_path = 'python'
endif

"Plug 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger = '<c-j>'
let g:UltiSnipsSnippetsDir = '~/.vim/UltiSnips'

" ======================== Autocommands ========================

" ---------- Python config if python stuff isn't working
" Python file config according to PEP8
"au BufNewFile,BufRead *.py
"    \ set tabstop=4
"    \ set softtabstop=4
"    \ set shiftwidth=4
"    \ set textwidth=79
"    \ set expandtab
"    \ set autoindent
"    \ set fileformat=unix
" Plugin 'vim-scripts/indentpython.vim'
"
" Flag bad whitespace
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
"

augroup configgroup
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
    autocmd BufWritePre *.hs
        \,*.java
        \,*.js
        \,*.json
        \,*.jsx
        \,*.md
        \,*.php
        \,*.py
        \,*.txt
        \,*.yml,*.yaml
        \
        \,.gitconfig
        \,*Jenkinsfile
        \,*Dockerfile
        \,*rc
        \ call <SID>StripTrailingWhitespaces()
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    "autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
    autocmd FileType php setlocal expandtab
    autocmd FileType php setlocal list
    "autocmd FileType php setlocal listchars=tab:+\ ,eol:-
    autocmd FileType php setlocal listchars=tab:\ \
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd FileType sh setlocal tabstop=4
    autocmd FileType sh setlocal shiftwidth=4
    autocmd FileType sh setlocal softtabstop=0
    autocmd FileType sh setlocal noexpandtab
    autocmd FileType javascript setlocal tabstop=2
    autocmd FileType javascript setlocal shiftwidth=2
    autocmd FileType javascript setlocal softtabstop=2
    autocmd FileType javascript setlocal suffixesadd=.js,.jsx,.ts,.tsx
    autocmd FileType typescript setlocal suffixesadd=.js,.jsx,.ts,.tsx
    autocmd FileType typescriptreact setlocal suffixesadd=.js,.jsx,.ts,.tsx
    autocmd FileType json setlocal tabstop=2
    autocmd FileType json setlocal shiftwidth=2
    autocmd FileType json setlocal softtabstop=2
    autocmd FileType markdown setlocal tabstop=4
    autocmd FileType markdown setlocal shiftwidth=4
    autocmd FileType markdown setlocal softtabstop=4
augroup END

set autoread
augroup autoreading
    autocmd!
    " Triger `autoread` when files changes on disk
    " https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
    " https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
    autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
    " Notification after file change
    " https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
    autocmd FileChangedShellPost *
      \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None
augroup END

" ======================== Custom functions ========================

" toggle between number and relativenumber
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc

function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

function! RepeatChar(char, count)
  return repeat(a:char, a:count)
endfunction

"https://vi.stackexchange.com/questions/12250/is-there-vim-plugin-that-can-show-keystroke-maps-or-abbreviations-for-command-af
function! ShowMappings()
    let cmd = histget(":", -2)
    echo 'Mappings for ' . cmd
    execute 'filter /' . cmd . '/ map'
endfunction
command! ShowMappings call ShowMappings()
"cnoremap <CR> <CR>:call ShowMappings()<CR>

"https://stackoverflow.com/questions/10884520/move-file-within-vim
function! MoveFile(newspec)
    let old = expand('%')
    " could be improved:
    if (old == a:newspec)
        return 0
    endif
    exe 'sav' fnameescape(a:newspec)
    call delete(old)
endfunction
command! -nargs=1 -complete=file -bar MoveFile call MoveFile('<args>')

command! -nargs=0 DeleteFile call delete(expand('%')) <Bar> bdelete!

" http://vim.wikia.com/wiki/Auto_highlight_current_word_when_idle
" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
nnoremap z/ :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>
function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    "echo 'Highlight current word: off'
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    "echo 'Highlight current word: ON'
    return 1
  endif
endfunction
"call AutoHighlightToggle()

" To edit a macro - :let @a='<C-R><C-R>a'
" Doesn't work for macros ending in <CR> or <NL>
" - Need to add a no-op like <Esc>
" The double <C-R> is there to enter the register literally
function! EditBuffer(buffer)
    call feedkeys(':let @' . a:buffer . "='\<C-R>\<C-R>" . a:buffer . "'", 'n')
endfunction
command! -nargs=1 EditBuffer call EditBuffer('<args>')
nnoremap Q :call EditBuffer(nr2char(getchar()))<CR>

" ======================== Custom commands ========================

command! VimEdit edit $MYVIMRC
command! VimSource source $MYVIMRC
"command! SnippetsEdit execute 'echo' '"~/.vim/snippets/'.&filetype.'"'
command! ChmodX !chmod +x "%:p"

" ======================== Mappings ========================
" ======== Maps ========

" Map Y to yank until EOL rather than act as yy
map Y y$

" ======== NoReMaps ========
" -------- multiple (Normal, Visual, Select, Operator-pending) --------

" Navigate to next buffers
noremap <C-J> :bprevious<CR>
noremap <C-K> :bnext<CR>
noremap <F7> :bprevious<CR>
noremap <F8> :bnext<CR>

" Navigate to next windows
noremap <C-H> <C-W>W
noremap <C-L> <C-W>w

" Add mouse movement to the jump list
noremap <LeftMouse> m'<LeftMouse>

" Easier repeat last command
noremap ,. @:

" -------- c --------

" Force saving files that require root permission
cnoremap w!! w !sudo tee % > /dev/null

cnoremap bd<CR> bp \| bd#<CR>

" From https://www.reddit.com/r/vim/comments/1yfzg2/does_anyone_actually_use_easymotion/
"" allows incsearch highlighting for range commands
"cnoremap $t <CR>:t''<CR>
"cnoremap $T <CR>:T''<CR>
"cnoremap $m <CR>:m''<CR>
"cnoremap $M <CR>:M''<CR>
"cnoremap $d <CR>:d<CR>``

" Make the pwd follow when you change buffers
"set autochdir
" or, if this gets annoying, use this
" http://vim.wikia.com/wiki/Easy_edit_of_files_in_the_same_directory
" to use %% to expand to the current file's directory
cabbr <expr> %% expand('%:p:h')

cnoremap cd<CR> cd %:p:h<CR>

" -------- n --------

" Insert single character
nnoremap <Space> :<C-U>exec "normal i".RepeatChar(nr2char(getchar()), v:count1)<CR>

" move vertically by visual line
"nnoremap j gj
"nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]

" Find empty brackets and quotes
nnoremap <S-K> /\m(\_s*)\\|<\_s*>\\|\[\_s*\]\\|{\_s*}\\|""\\|''\\|``<CR>:noh<CR>l

" Tab and Backspace navigate the jump list
" (and add the current location to the jump list before leaving)
nnoremap <BS> <C-O>

" Add current location to jump list
nnoremap `` m`

" Add jk motions to the jump list
nnoremap <expr> k (v:count > 1 ? "m'" . v:count : '') . 'k'
nnoremap <expr> j (v:count > 1 ? "m'" . v:count : '') . 'j'

" Insert camelCase
"nnoremap cri ~li

" -------- n and v --------

" Swap jump-to-mark and change-case
"nnoremap ` ~
"vnoremap ` ~
"nnoremap ~ `
"vnoremap ~ `

" Swap enter-command and next-character-found
nnoremap : ;
vnoremap : ;
nnoremap ; :
vnoremap ; :

" Swap jump-to-mark and select-paste-buffer
"nnoremap ' "
"vnoremap ' "
"nnoremap " '
"vnoremap " '

" Execute current file
nnoremap <leader>x :!"%:p"<enter>

" -------- v --------

"http://vim.wikia.com/wiki/Search_for_visually_selected_text
vnoremap // y/\V<C-R>"<CR>
"vnoremap <expr> // 'y/\V'.escape(@",'\').'<CR>'

" Hightlight line content - works with a count too
vnoremap v jk$ho^

" Edit file in selection in js import statements
vnoremap gf "fy:e %:p:h/<C-R><C-R>f

" -------- i --------

" Separate the undo sequence on <CR>
"inoremap <CR> <C-G>u<CR>
" This breaks delimitmate and maybe other plugins too?

" jk is escape
inoremap jk <esc>
"vnoremap jk <esc> " really annoying. I don't think I need it

" save buffer
inoremap ;w<CR> <ESC>:w<CR>

" Finally load ~/.vim/after/plugin/final_vimrc.vim

