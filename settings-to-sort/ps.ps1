# Useful for getting credentials
# .\git-credential-winstore.exe get
# protocol=https
# host=github.com

# Set up console
if((Test-Path('~/j')) -and ((Get-Item ~).FullName -eq (pwd).Path)){
    Set-Location ~/j
    $host.UI.RawUI.BackgroundColor = "black"
    Clear-Host
}

# User defined functions and aliases

#Commandlet Function example/skeleton
#Function commandlet-name {
#    [cmdletBinding()]
#    param()
#    BEGIN{}
#    PROCESS{
#        Some Code Here
#            Try{
#                $continue=$true
#                Some-commandlet -ErrorVariable xxx -ErrorAction Stop
#                Some Code Here
#                }Catch{
#                $Continue=$false
#                 Some Code Here
#                }
#
#            IF($continue){
#                Some Code Here
#            }
#
#    }
#    END{}
#}


Function Get-EmptyFolders {
    PROCESS{
        ls -recurse | ? {$_.PSIsContainer} | ? {$_.GetFiles().Count -eq 0 -and $_.GetDirectories().Count -eq 0} # | select FullName
    }
}

# Come back to this if needed?
#function invoke-assuperuser { Start-Process powershell -Verb runAs $args }
#set-alias sudo invoke-assuperuser

function eeConfig {
    param(
        [ValidateSet(
            "atom",
            "git",
            "eslintrc",
            "eslintrc-endgame"
        )]
        [Parameter(Mandatory=$true)]
        [string] $data,
        [ValidateSet(
        "load",
        "save",
        "erase"
        )]
        [Parameter(Mandatory=$true)]
        [string] $operation
    )
    begin {}
    process {
        switch ($data) {
            "atom" {
                $live = "~/.atom"
                $stored = "~/j/settings/atom"
            }
            "git" {
                $live = "~/.gitconfig"
                $stored = "~/j/settings/git/.gitconfig"
            }
            "eslintrc" {
                $live = "~/.eslintrc"
                $stored = "~/j/settings/eslint/personal/.eslintrc"
            }
            "eslintrc-endgame" {
                $live = "C:/Workspaces_Jesse/.eslintrc"
                $stored = "~/j/settings/eslint/endgame/.eslintrc"
            }
        }

        switch ($operation) {
            "load" {
                Get-ChildItem $stored | %{cp $_.FullName $live}
            }
            "save" {
                Get-ChildItem $live | %{cp $_.FullName $stored}
            }
            "erase" {
                Remove-Item -Recurse -Force $live
            }
        }
    }
    end {}
}

function Run-Accurev {
    param(
        [ValidateSet(
            "add",
            "commit",
            "fetch",
            "pull",
            "status",
            "login",
            "push",
            "view-push"
        )]
        [Parameter(Mandatory=$true)]
        [string] $Operation,
        [string] $Parameter1,
        [string] $Parameter2
    )
    begin {}
    process {
        switch($Operation) {
            "add" {
                accurev add -x 2>$null
                accurev keep -n -c . 2>$null
                accurev stat -M -fl | %{$toDefunct += ,$_}
                accurev defunct @toDefunct
                # if the above doesn't work, run the defunct twice, but filter first on files, second on directories
            }
            "commit" {
                accurev promote -k -I $Parameter1
            }
            "fetch" {
                accurev update -i
            }
            "pull" {
                accurev update
            }
            "status" {
                accurev stat --outgoing -fl
            }
            "login" {
                accurev login -n
            }
            "push" {
                $repoSuffix = "." + (accurev info | sls -Pattern "Basis:" | %{([string]$_).Split(".", 2)[1]})
                $from = $Parameter1 + $repoSuffix
                $to = $Parameter2 + $repoSuffix
                accurev mergelist -s $from -S $to -fl | %{$toPromote += ,$_}
                accurev promote -s $from -S $to @toPromote
            }
            "view-push" {
                $repoSuffix = "." + (accurev info | sls -Pattern "Basis:" | %{([string]$_).Split(".", 2)[1]})
                $from = $Parameter1 + $repoSuffix
                $to = $Parameter2 + $repoSuffix
                accurev mergelist -s $from -S $to -fl
            }
        }
    }
}
Set-Alias acc Run-Accurev

function Out-Toast {
    param(
        [parameter(
            Mandatory=$true,
            ValueFromPipeline=$True)]
        [AllowEmptyString()]
        [string]$Message,
        [string]$Title
    )
    BEGIN {
        [void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
        $objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon

        $objNotifyIcon.Icon = "C:\Windows\System32\PerfCenterCpl.ico"
        $objNotifyIcon.BalloonTipIcon = "Info"
        $objNotifyIcon.BalloonTipTitle = $Title

        $lastMessageTime = Get-Date
    }
    PROCESS {
        if ($Message -eq "") { return }

        if (((Get-Date) - $lastMessageTime).TotalMilliseconds -gt 5000) {
            $objNotifyIcon.BalloonTipText = $Message
        } else {
            $objNotifyIcon.BalloonTipText = $objNotifyIcon.BalloonTipText + "`n" + $Message
        }

        $objNotifyIcon.Visible = $True
        $objNotifyIcon.ShowBalloonTip(10000)

        $lastMessageTime = Get-Date
    }
    END {
        # Start-Sleep -Milliseconds 5000
        # $objNotifyIcon.Dispose()
    }
}

function Clone-EedrahRepo {
    param(
    [string] $repoName
    )

    if ($repoName){
        git clone "https://github.com/eedrah/$($repoName)"
        cd $repoName
    } else {
        $result = Invoke-BasicAuthenticatedRequest -Uri "https://api.github.com/user/repos?type=owner&per_page=100" -Method Get
        $repos = ConvertFrom-Json($result.Content)
        foreach($repo in $repos){
            $repo.name
        }
    }
}

function Get-Clip {
    Get-Clipboard -Raw
}

function Set-Clip {
    param(
        [parameter(
            Mandatory=$true,
            ValueFromPipeline=$True)]
        [AllowEmptyString()]
        [string]$string
    )
    BEGIN {
        [string[]] $array = @()
    }
    PROCESS {
        $array += $string
    }
    END {
        Add-Type -AssemblyName presentationcore
        $clipboard = $array -join [Environment]::NewLine
        [System.Windows.Clipboard]::SetText($clipboard)
    }
}

function AutoCommitAndPush-ToOriginMaster {
    while($true){
        if(git status --porcelain){
            git add -A
            CommitAndPush-ToOriginMaster("autocommit")
        }
        sleep 1
    }
}

function CommitAndPush-ToOriginMaster {
    param(
        $message
    )
    git commit -m $message
    git push origin head:master
    if($LASTEXITCODE -ne 0){
        git pull origin master:master
        git merge master
        git push origin head:master
    }
}


function Watch-FromOriginMaster {
    function getRemoteSha {
        return (git ls-remote origin master).split()[0]
    }

    $sha = ""

    while($true){
        $remoteSha = getRemoteSha
        if($sha -ne $remoteSha){
            $sha = $remoteSha
            git pull origin master:master
            git merge master
        }
        sleep 1
    }
}


function Delete-TracesOfEedrah (){
    [CmdletBinding(ConfirmImpact = 'High', SupportsShouldProcess)]
    param()
    # Remove j folder
    if(Test-Path('~/j')){
        $jFolder = Get-Item ~/j
        if($PSCmdlet.ShouldProcess($jFolder, 'Delete')){
            rm $jFolder -Recurse -Force
        }
    }

    # Restore gitconfig if eedrah is user, optionally delete backup
    if ((Get-GitUser) -eq "eedrah"){
         if($PSCmdlet.ShouldProcess(".gitconfig", "Delete user 'Eedrah'")) {
            Config-Git -Erase
            if (Test-Path('~/.gitconfig_backup')){
                Config-Git -RestoreBackup
                $gitConfig = Get-Item ~\.gitconfig
                $gitUser = Get-GitUser
                if($PSCmdlet.ShouldProcess($gitConfig, "Delete with user '$gitUser'")){
                    Config-Git -Erase
                }
            }
        }
    }

    # Uninstall chocolately packages
    $packages = Get-Package -ProviderName chocolatey
    if($packages){
        foreach($package in $packages){
            if($PSCmdlet.ShouldProcess($package.Name, 'Uninstall')){
                Uninstall-Package $package
            }
        }
    }

    # Remove GitHub credentials
    if ((cmdkey /list:git:https://github.com | findstr eedrah) -and $PSCmdlet.ShouldProcess("GitHub.com", "Delete credentials for Eedrah")){
        cmdkey /delete:git:https://github.com
    }
}

function Get-GitUser {
    return git config --global user.name
}

function Has-BOM{
    return $input | where {
            !$_.PsIsContainer -and $_.Length -gt 2
        } | where {
            $contents = new-object byte[] 3
            $stream = [System.IO.File]::OpenRead($_.FullName)
            $stream.Read($contents, 0, 3) | Out-Null
            $stream.Close()
            $contents[0] -eq 0xEF -and $contents[1] -eq 0xBB -and $contents[2] -eq 0xBF
        }
}

function Remove-BOM {
    param(
        [parameter(
            Mandatory=$True,
            ValueFromPipeline=$True)]
        $File
    )
    PROCESS {
        if(!($File -is [System.IO.FileInfo])){
            $File = Get-Item $File
        }
        $text = Get-Content $File.FullName
        [System.IO.File]::WriteAllLines($File.FullName, $text)
    }
}

function Run-EedrahScript(){
    param(
        [Parameter(
            ValueFromPipeline=$true)]
        [string]$Path,
        [switch]$Look = $false
    )
    process{
        $uri = "https://api.github.com/repos/eedrah/settings/contents/" + $Path
        $response = Invoke-WebRequest -Uri $uri
        $listing = $response.Content | ConvertFrom-Json
        if($listing -is [Array]){
            $contents = @()
            foreach($item in $listing){
                $contents += $item.Name
            }
            return $contents
        } else {
            $url = $listing.download_url
            $file = Invoke-WebRequest -Uri $url
            $fileContents = $file.Content
            if($Look.IsPresent){
                return $fileContents
            } else {
                Invoke-Expression $file
            }
        }
    }
}

function Remove-ItemWithLongPath(){
    [CmdletBinding(ConfirmImpact = 'High', SupportsShouldProcess)]
    param(
        [Parameter(Mandatory, ValueFromPipeline)]
        [SupportsWildcards()]
        [ValidateNotNullOrEmpty()]
        [string[]] $Path
    )
    process{
        # First round trying to delete
        Get-ChildItem $Path -Recurse -ErrorAction SilentlyContinue | Remove-Item -Recurse -Force -ErrorAction SilentlyContinue

        # For the long paths, do second round
        if (Test-Path($Path)) {
            $FilesAndFolders = Get-Item $Path
            foreach($Item in $FilesAndFolders){
                if($PSCmdlet.ShouldProcess($Item, 'Delete')){
                    $emptyDirName = [System.Guid]::NewGuid().ToString()
                    $emptyDir = New-Item -Type Directory -Name $emptyDirName

                    robocopy $emptyDir $Item /mir
                    Remove-Item $Item

                    Remove-Item $emptyDir
                }
            }
        }
    }
}
Set-Alias nuke Remove-ItemWithLongPath

function Initialize-GitHubRepo(){
    param(
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true)]
        [string]$Name,

        [System.Management.Automation.CredentialAttribute()]
        $Credential
    )
    BEGIN{
        if(!$Credential){
            $Credential = Get-Credential -Message "Enter your Github credentials" -Username "eedrah"
        }
        $initialDirectory = pwd;
    }
    PROCESS{
        cd $initialDirectory
        md $Name | cd
        git init
        Create-GitHubRepo -FolderName -Credential $Credential
        "# " + ((pwd).Path | Split-Path -Leaf) | Out-Utf8File README.md
        git add README.md
        git commit -m "initial commit of README.md"
        git push -u
    }
}

function Create-GitAttributesFile(){
    "* text=auto" | Out-Utf8File .gitattributes
}

function Convert-FileToUtf8(){
    param(
        [parameter(
            Mandatory=$True,
            ValueFromPipeline=$True)]
        $File
    )
    PROCESS {
        if(!($File -is [System.IO.FileInfo])){
            $File = Get-Item $File
        }
        $Content = Get-Content $File.FullName
        [System.IO.File]::WriteAllLines($File.FullName, $Content)
    }
}

function Out-Utf8File(){
    param(
        [parameter(Mandatory=$true)]
        [string]$File,
        [parameter(
            Mandatory=$true,
            ValueFromPipeline=$True)]
        [string]$Content
    )
    Out-File -FilePath $File -Encoding utf8 -InputObject $Content -Append
    Get-Item $File | Has-BOM | Remove-BOM
}

function Append-LastCommandTo(){
    param(
        [Parameter(Mandatory=$True)]
        [string]$File
    )
    (Get-History -Count 1).CommandLine | Out-Utf8File $File
}

function Monitor-Git {
    $gitMonitor = Start-Job {
        cd $args[0]
        Add-Type -AssemblyName System.Speech
        function speak {
            if((git last)){
                $results = "What are you doing?"
            } else {
                $results = "You need to " + (git what)
            }
            (New-Object -TypeName System.Speech.Synthesis.SpeechSynthesizer).speak($results)
        }
        while($true){
            speak
            start-sleep -s 600
        }
    } -arg $pwd
    return $gitMonitor
}

function Start-JobHere ([ScriptBlock]$block) {
    Start-Job -Init ([ScriptBlock]::Create("cd $pwd")) -Script $block
}
Set-Alias sj Start-JobHere

function Receive-AllJobs {
    $color = ((Get-Host).PrivateData).VerboseForegroundColor
    ForEach($job in Get-Job | Where{$_.HasMoreData}){
        Write-Host -ForegroundColor $color "Output so far from command:"
        Write-Host -ForegroundColor $color ($job.ChildJobs).Command
        Write-Host -ForegroundColor $color "----------------"
        Write-Output (Receive-Job($job.Id))
        Write-Host -ForegroundColor $color "----------------"
        if($job.HasMoreData){
            Write-Host -ForegroundColor $color "More output is coming..."
        }
    }
}
Set-Alias rj Receive-AllJobs

function Download-GitIgnore {
    param
    (
        [ValidateSet(
            "Actionscript",
            "Ada",
            "Agda",
            "Android",
            "AppEngine",
            "AppceleratorTitanium",
            "ArchLinuxPackages",
            "Autotools",
            "Bancha",
            "C",
            "C++",
            "CFWheels",
            "CMake",
            "CUDA",
            "CakePHP",
            "ChefCookbook",
            "Clojure",
            "CodeIgniter",
            "CommonLisp",
            "Composer",
            "Concrete5",
            "Coq",
            "CraftCMS",
            "DM",
            "Dart",
            "Delphi",
            "Drupal",
            "EPiServer",
            "Eagle",
            "Elisp",
            "Elixir",
            "Erlang",
            "ExpressionEngine",
            "ExtJS-MVC",
            "ExtJs",
            "Fancy",
            "Finale",
            "ForceDotCom",
            "Fortran",
            "FuelPHP",
            "GWT",
            "GitBook",
            "Go",
            "Gradle",
            "Grails",
            "Haskell",
            "IGORPro",
            "Idris",
            "Java",
            "Jboss",
            "Jekyll",
            "Joomla",
            "Jython",
            "Kohana",
            "LabVIEW",
            "Laravel",
            "Leiningen",
            "LemonStand",
            "Lilypond",
            "Lithium",
            "Lua",
            "Magento",
            "Maven",
            "Mercury",
            "MetaProgrammingSystem",
            "Meteor",
            "Nim",
            "Node",
            "OCaml",
            "Objective-C",
            "Opa",
            "OracleForms",
            "Packer",
            "Perl",
            "Phalcon",
            "PlayFramework",
            "Plone",
            "Prestashop",
            "Processing",
            "Python",
            "Qooxdoo",
            "Qt",
            "R",
            "ROS",
            "Rails",
            "RhodesRhomobile",
            "Ruby",
            "Rust",
            "SCons",
            "Sass",
            "Scala",
            "Scrivener",
            "Sdcc",
            "SeamGen",
            "SketchUp",
            "SugarCRM",
            "Swift",
            "Symfony",
            "SymphonyCMS",
            "Target3001",
            "Tasm",
            "TeX",
            "Textpattern",
            "TurboGears2",
            "Typo3",
            "Umbraco",
            "Unity",
            "VVVV",
            "VisualStudio",
            "Waf",
            "WordPress",
            "Xojo",
            "Yeoman",
            "Yii",
            "ZendFramework",
            "Zephir",
            "gcov",
            "nanoc",
            "opencart",
            "stella")]
        [string]$Language
    )

    $headers = @{Accept = "application/vnd.github.v3.raw+json"}

    if($Language){
        $uri = "https://api.github.com/gitignore/templates/$Language"
        $response = Invoke-WebRequest -Uri $uri -Headers $headers -Method Get -OutFile ".gitignore"
    } else {
        $response = Invoke-WebRequest -Uri "https://api.github.com/gitignore/templates" -Headers $headers -Method Get
        return ConvertFrom-Json($response.Content)
    }
}

function Download-GitIgnore-VS {
    # Deprecated - here for compatibility with Powershell 2.x
    (new-object System.Net.WebClient).Downloadfile("https://raw.github.com/github/gitignore/master/VisualStudio.gitignore", "$pwd\.gitignore")
}

function Invoke-BasicAuthenticatedRequest {
    param (
        [Parameter(Mandatory=$true)] [string] $Uri,
        [Hashtable] $Body,
        [ValidateSet('Get', 'Post')] [string] $Method,
        [System.Management.Automation.CredentialAttribute()] $Credential
    )
    if(!$Credential){
        $Credential = Get-Credential -Message "Enter your credentials"
    }

    $username = $Credential.GetNetworkCredential().Username
    $pair = "$($username):$($Credential.GetNetworkCredential().Password)"
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    $base64 = [System.Convert]::ToBase64String($bytes)
    $basicAuthValue = "Basic $base64"
    $headers = @{ Authorization = $basicAuthValue }

    $result = Invoke-WebRequest -Uri $Uri -Headers $headers -Method $Method -Body (ConvertTo-Json $Body)
    return $result
}

function Create-GitHubRepo(){
    param
    (
        [Parameter(Mandatory=$True, Position=1, ParameterSetName="GivenName")]
        [string]$Name,
        [Parameter(ParameterSetName="FolderName")]
        [switch]$FolderName = $false,

        [string]$RemoteName = "origin",
        [System.Management.Automation.CredentialAttribute()]
        $Credential
    )
    if(!$Credential){
        $Credential = Get-Credential -Message "Enter your Github credentials" -Username "eedrah"
    }

    if($PSCmdlet.ParameterSetName -eq "FolderName"){
        $Name = (pwd).Path | Split-Path -Leaf
    }

    $body = @{ name = $Name}
    $uri = "https://api.github.com/user/repos"

    $response = Invoke-BasicAuthenticatedRequest -Uri $uri -Method Post -Body $body -Credential $Credential

    $remoteUrl = (ConvertFrom-Json($response.Content)).clone_url
    git remote add $RemoteName $remoteUrl
}

function SomethingToDoWithSettingsImport(){
    # Need to:

    # look at gitconfig
    # save gitconfig
    # load gitconfig
    #   - from repo
    #    - from web
    # backup other gitconfig
    # reload backup

    # Maybe apply for other files also?
    # - gitconfig
    # - powershell $profile
    # - bashrc
    # - vimrc
}

function Config-Git(){
    param(
        [switch] $Look = $false,
        [switch] $Save = $false,
        [switch] $Load = $false,
        [switch] $Download = $false,
        [switch] $Erase = $false,
        [switch] $RestoreBackup = $false,
        [switch] $DeleteBackup = $false
    )

    $ErrorActionPreference = "Stop"

    $configPath = '~\.gitconfig'
    $masterPath = '~\j\settings\git\.gitconfig'
    $backupPath = $configPath + '_backup'
    $downloadPath = 'https://raw.github.com/eedrah/settings/gh-pages/git/.gitconfig'

    $numberOfSwitches = [int]$Look.IsPresent + [int]$Save.IsPresent + [int]$Load.IsPresent + [int]$Download.IsPresent + [int]$Erase.IsPresent + [int]$RestoreBackup.IsPresent + [int]$DeleteBackup.IsPresent
    if($numberOfSwitches -gt 1){
        throw 'Cannot have more than one operation'
    } elseif ($numberOfSwitches -lt 1) {
        throw 'Must have an operation'
    }

    if($Save.IsPresent){
        if (!(Test-Path($configPath))){
            throw "File $configPath does not exist."
        } elseif (!(Test-Path($masterPath))){
            throw "File $masterPath does not exist."
        } else {
            cp $configPath $masterPath
        }
    } elseif ($Load.IsPresent){
        if(!(Test-Path($masterPath))){
            throw "File $masterPath does not exist."
        } else {
            if (Test-Path($configPath)){
                if (Test-Path($backupPath)){
                    throw "File $backupPath already exists. Either Erase current config to Load without backing up or DeleteBackup."
                }
                cp $configPath $backupPath
            }
            cp $masterPath $configPath
        }
    } elseif ($Erase.IsPresent){
        rm $configPath
    } elseif ($Download.IsPresent){
        if (Test-Path($configPath)){
            if (Test-Path($backupPath)){
                throw "File $backupPath already exists. Either Erase current config to Load without backing up or DeleteBackup."
            }
            cp $configPath $backupPath
        } else {
            New-Item -ItemType File $configPath
        }
        $webClient = New-Object System.Net.WebClient
        $webClient.Downloadfile($downloadPath, (Resolve-Path($configPath)).Path)
    } elseif ($RestoreBackup.IsPresent){
        if (!(Test-Path($backupPath))){
            throw "File $backupPath does not exist."
        } else {
            cp $backupPath $configPath
            rm $backupPath
        }
    } elseif ($DeleteBackup.IsPresent){
        if (!(Test-Path($backupPath))){
            throw "File $backupPath does not exist."
        } else {
            rm $backupPath
        }
    } elseif ($Look.IsPresent){
        if (!(Test-Path($configPath))){
            throw "File $configPath does not exist."
        } else {
            cat $configPath
        }
    }
}

function Reload-Profile(){
    $currentDirectory = $pwd
    . $PROFILE
    cd $currentDirectory
}

# Aliases that change functionality
if (Get-Command less -errorAction SilentlyContinue)
{
    Set-Alias more less
}

# Aliases for convenience
Set-Alias subl 'C:\Program Files\Sublime Text 3\sublime_text.exe'
Set-Alias markdown 'C:\Program Files (x86)\MarkdownPad 2\MarkdownPad2.exe'
Set-Alias vs "C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\devenv.exe"

$chromePath = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
if(Test-Path($chromePath)){
    Set-Alias chrome $chromePath
}
$chromePath = "~\AppData\Local\Google\Chrome\Application\chrome.exe"
if(Test-Path($chromePath)){
    Set-Alias chrome $chromePath
}

# TODO
# Create function to determine/select if path exists and set aliases
# Deal with the path: subl 2
# Perhaps with arrays of possible locations for things, and then it chooses the one that exists first?
# Think about importing the Posh-git profile into here? This does seem to be my main script now... But what about when they update the default posh git profile?

# Set up environment
$gitPath = 'C:\Program Files (x86)\Git\bin'
if (!(Test-Path($gitPath))) {$gitPath = 'C:\Program Files\Git\bin'}
if(Test-Path($gitPath)){
    $env:Path += ";$gitPath"
}

$nvmPath = "~\AppData\Roaming\nvm"
if (Test-Path($nvmPath)) {$env:Path += ";$((Get-Item $nvmPath).FullName)"}

$nodePath = "$env:SystemDrive\Program Files\nodejs"
if (!(Test-Path($nodePath))) {$nodePath = "$env:SystemDrive\Program Files (x86)\nodejs";}
if (Test-Path($nodePath)) {$env:Path += ";$nodePath"}

# Import tools
$poshGitProfilePath = $PROFILE + "\..\poshgit.ps1"
if (Test-Path $poshGitProfilePath) {
    . $poshGitProfilePath
}
