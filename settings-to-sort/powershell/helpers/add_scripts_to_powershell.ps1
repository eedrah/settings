if (!(Test-Path $profile)) {
    New-Item -Path $profile -Type file -Force
}
@"
if(Test-Path "~\j\settings\ps.ps1"){
    . ~\j\settings\ps.ps1
}
"@  >> $profile

$poshGitProfilePath = $PROFILE + "\..\poshgit.ps1"
if (!(Test-Path $poshGitProfilePath)) {
    $poshGitProfile = (New-Object net.webclient).DownloadString("https://raw.githubusercontent.com/dahlbyk/posh-git/master/profile.example.ps1")
    $poshGitProfile.Replace(".\posh-git", "posh-git").Replace("Start-SshAgent -Quiet", "") > $poshGitProfilePath
}
