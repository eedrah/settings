adduser nisl
usermod -aG sudo nisl
sed -i 's/^PasswordAuthentication no$/PasswordAuthentication yes\nAllowUsers nisl/' /etc/ssh/sshd_config
service sshd reload

su - nisl
#sudo snap install docker # Now deprecated
sudo apt-get update
sudo apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update
sudo apt-get install -y docker-ce

#sudo systemctl status docker
sudo docker run --name hello-world hello-world

# Add user to docker group so to not need sudo to run docker
sudo usermod -aG docker $USER
su - $USER
docker rm hello-world
docker image rm hello-world

COMPOSE_VERSION=1.21.2 # Check from https://docs.docker.com/compose/install/#install-compose
sudo curl -L https://github.com/docker/compose/releases/download/$COMPOSE_VERSION/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version

ssh-keygen
cat ~/.ssh/id_rsa.pub
# Save to github
eval $(ssh-agent) && ssh-add
#git clone git@github.com:.../...
