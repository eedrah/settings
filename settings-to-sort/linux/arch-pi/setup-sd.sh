sudo fdisk /dev/mmcblk0
# opn<Enter><Enter><Enter>+100M
# tc
# n<Enter><Enter><Enter><Enter>
# w

sudo mkfs.vfat /dev/mmcblk0p1 
sudo mkfs.ext4 /dev/mmcblk0p2

mkdir /tmp/arch
cd /tmp/arch
mkdir root
sudo mount /dev/mmcblk0p2 root
mkdir root/boot
sudo mount /dev/mmcblk0p1 root/boot

wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-latest.tar.gz
md5sum ArchLinuxARM-rpi-latest.tar.gz 
sudo bsdtar -xpf ArchLinuxARM-rpi-latest.tar.gz -C root
sync
cd root/

echo "dtoverlay=dwc2" | sudo tee -a boot/config.txt 
echo "dwc2" | sudo tee -a etc/modules-load.d/raspberrypi.conf 
echo "g_ether" | sudo tee -a etc/modules-load.d/raspberrypi.conf 
#sudo sed -i 's/rootwait/rootwait modules-load=dwc2,g_ether/' boot/cmdline.txt 
cat | sudo tee etc/systemd/network/20-wired.network <<EOF
[Match]
Name=usb0

[Network]
Address=192.168.10.2/24
EOF

# sed -i 's/alarm/myUser/' in all of /etc passwd shadow gpasswd gshadow
# change home directory
# change hostname

# Plug it in

IF_NAME=$(dmesg --notime | tail -5 | grep 'renamed from usb0' | cut -d ' ' -f 3 | cut -d : -f 1)
sudo ip address add 192.168.10.1/24 dev "$IF_NAME"
sudo ip link set "$IF_NAME" up
ping 192.168.10.2

ssh myUser@192.168.10.2
