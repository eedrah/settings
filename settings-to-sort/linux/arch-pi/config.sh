sudo vi /etc/locale.conf 
sudo vi /etc/locale.gen 
sudo locale-gen
sudo localectl

wpa_passphrase ssid password | sudo tee /etc/wpa_supplicant/wpa_supplicant-wlan0.conf

sudo cp /etc/systemd/network/{eth0.network,wlan0.network}
sudo sed -i 's/eth0/wlan0/' /etc/systemd/network/wlan0.network
systemctl enable --now wpa_supplicant@wlan0

mkdir $HOME/.ssh
chmod 700 $HOME/.ssh
# scp something.pub to /home/myUser/.ssh/authorized_keys 
chmod 0644 $HOME/.ssh/authorized_keys 

echo "auth optional pam_faildelay.so delay=4000000" | sudo tee -a /etc/pam.d/system-login
echo "PermitRootLogin no" | sudo tee -a /etc/ssh/sshd_config
sudo systemctl restart sshd

sudo pacman -S cronie
sudo systemctl enable --now cronie

sudo pacman -S tmux
sudo pacman -S avahi
sudo pacman -S git
sudo pacman -S ldns # drill - replacement for dig
sudo pacman -S rsync
sudo pacman -S expect
sudo pacman -S mutt

git clone https://github.com/eedrah/settings
